const fs = require('fs')
const fs_readdir = require("fs-readdir-recursive");
const readline = require('readline-promise').default;

const { createClient: redisClient } = require('redis');

const redis = redisClient({
  url: 'redis://127.0.0.1:6380'
})

redis.on('error', err => logger.info('Redis Client Error', err));

async function restore (folderPath) {

  let files = fs_readdir(`output/${folderPath}`);
  

  for (let i = 0; i < files.length; i++) {
    files_split = files[i].split("_")
    let key = ""
    for (let j = 1; j < files_split.length - 2; j++) {
      key += `${files_split[j]}_`
    }
    key = key.slice(0,-1)
    
    console.info(`key: ${key}`)
    await redis.del(key)
    console.log('after delete key')
    
    const fileStream = fs.createReadStream(`output/${folderPath}/${files[i]}`);
    const lineReader = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });
    
    try {
      let arrayData = [];
      for await (const line of lineReader) {
        // console.log(`line: ${line}`)
        let obj = JSON.parse(line)
        arrayData.push(obj);
        // console.log('BEFORE ADD KEY: ' + obj.id)
        // await redis.zAdd(key, {score: obj.score, value: obj.id})
        // console.log(values)

      }
      console.log("before push to array")
      for (let k = 0; k < arrayData.length; k++) {
        console.log('BEFORE ADD KEY: ' )
        await redis.zAdd(key, {score: arrayData[k].score, value: arrayData[k].id})
        // console.log(values)
        console.log('AFTER ADD KEY: ' )

      }
    } catch (error) {
      console.error(error)      
    } finally {
      console.log('huuu')
      fileStream.close()
    }    



    

    console.info(`store complete key: ${key}`)
  }
}

async function main(){
  await redis.connect();

  await restore("test1");
  // await restore("littleboss");
  // await restore("grandboss");
  // await restore("endless");

  console.log("COMPPLETE ALL");
}

main();